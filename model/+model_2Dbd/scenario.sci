// model_2D/scenario.m
// 2D simulation : scenario change and set adequate parameter values
//
// Authors: 
//
// MOTTELET S. (UTC), stephane.mottelet@utc.fr
// LETURIA M. (UTC)
// MANAI M. S. (UTC)
//
// This file is hereby licensed under the terms of the GNU GPL v3.0,
// For more information, see the COPYING file which you should have received//
// This file is hereby licensed under the terms of the GNU GPL v3.0,
// For more information, see the COPYING file which you should have received
//
// Last modified : Thu Jul 20 11:49:34 CEST 2023

function scenario(tag)
  if argn(2)==0
    string=get(gcbo,'string');
    value=get(gcbo,'value');
    tag=string{value};
  end
  f=gcf();
  param=f.UserData.param;  
  inputParms={'flow','P_in'};
  param.scenario=tag;
  param.P_in_function=common.P_in;
  param.T_f_function=common.T_f;
  if ~strcmp(tag,'Constant P_in and max flow')
    param.P_in_flow_function=common.P_in_flow;
    enable(inputParms)
  elseif ~strcmp(tag,'User-defined P_in and max flow')
    param.P_in_flow_function=common.P_in_flow_user;
    disable(inputParms)
  end
  f.UserData.param=param;
  common.refresh;
end

function enable(parms)
  for i=1:length(parms)
    set(findobj(gcf(),'tag',parms{i}),'enable','on')
  end
end

function disable(parms)
  for i=1:length(parms)
    set(findobj(gcf(),'tag',parms{i}),'enable','off')
  end
end

function %s_set(h,p,v)
endfunction

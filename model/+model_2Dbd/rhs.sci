// model_2Db/rhs.sci
// 2D simulation
// note : some component of out(p.nnodes.ind_T) and out(p.nnodes.ind_P)
// are equal to an algebraic expression enforcing the boundary conditions.
//
// Authors: 
//
// MOTTELET S. (UTC), stephane.mottelet@utc.fr
// LETURIA M. (UTC)
// MANAI M. S. (UTC)
//
// This file is hereby licensed under the terms of the GNU GPL v3.0,
// For more information, see the COPYING file which you should have received//
// This file is hereby licensed under the terms of the GNU GPL v3.0,
// For more information, see the COPYING file which you should have received
//
// Last modified : Thu Jul 20 11:49:34 CEST 2023

function out=rhs(t,X,Xp,p)
  out   = zeros(X);  
  //
  P_v=X(p.nodes.ind_P_v,:); // pressure of valve 
  //
  innerflow=0;
  for i=1:length(p.dom) // for each layer/brick
    dom=p.dom{i};
    T=X(dom.ind_T,:); // all bricks have temperature dofs
    // laprT = d^2T/dr^2+1/r*dT/dr
    // lapzT = d^2T/dz^2        
    laprT=dom.lapr*T; lapzT=dom.lapz*T;
    // Normal derivatives
    dnrT=dom.dnr*T; dnzT=dom.dnz*T;
    // local node numbers (interior, boundary)
    int=dom.int; allbdy=dom.allbdy;
    //
    material=p.dom{i}.type;
    lambda_z=p(sprintf('lambda_%s_z',material));
    lambda_r=p(sprintf('lambda_%s_r',material));
    divLambdaT=lambda_r*laprT+lambda_z*lapzT;
    //
    if material=='mh' || material == 'duct' // hydrogen duct or metal hydrure layer/brick
      P       = X(dom.ind_P,:);
      P_over_T=P./T;
      rho_g   = p.M_g/p.R*1E5*P_over_T;
      if material == 'mh'
        rho_s   = X(dom.ind_rho_s,:);
        rhoCp_e = p.epsilon*rho_g*p.C_pg+(1-p.epsilon)*rho_s*p.C_ps;
        // mass flow and source terms
        [m,source] = common.mass_flow(T,P,rho_s,p);
        out(dom.ind_rho_s,:)=m/(1-p.epsilon);
        K=p.K_mh;
      else
        //lambda_z=0.187;// conductivity of dihydrogen at 300K
        lambda_z=p.epsilon*0.187+(1-p.epsilon)*p.lambda_steel_z;
        lambda_r=lambda_z;
        //rhoCp_e = rho_g*p.C_pg;
        rhoCp_e=p.epsilon*rho_g*p.C_pg+(1-p.epsilon)*p.C_psteel;
        source=0;
        m=0;
        K=p.K_duct;
      end
      mu_g0=9.05e-6; // dynamic viscosity
      alpha1=1e5*K/mu_g0/p.epsilon;
      alpha2=p.R/p.M_g/1e5/p.epsilon;
      alpha3=(1e10*K*p.M_g)/(mu_g0*p.R);  
      // Temperature@interior,
      dTdt=(divLambdaT+source)./rhoCp_e;
      // Pressure@interior, metal hydrure
      //mu_g=9.05e-6*(T/293).^0.68; // dynamic viscosity
      // Divergence term (to be adapted if anisotropic permeability)
      P_over_Tmu_g=P_over_T.*(T/293).^-0.68;
      div=(dom.gradr*P_over_Tmu_g).*(dom.gradr*P)+...
          (dom.gradz*P_over_Tmu_g).*(dom.gradz*P)+...
          P_over_Tmu_g.*(dom.lapr*P+dom.lapz*P);
      // In this formula dTdt is already computed above
      dPdt=dTdt.*P_over_T+T.*(alpha1*div-alpha2*m);
      out(dom.ind_P(int),:)=dPdt(int,:);
      // boundary, initialize value for default null flux condition
      // only relative permeability counts here (normalized by a reference K_ref)
      out(dom.ind_P(allbdy),:)=out(dom.ind_P(allbdy),:)+K/p.K_ref*(dom.dnr*P+dom.dnz*P); // P normal derivative
      //
      // hydrogen input
      //
      for j=dom.bdyInput 
        bdy=dom.bdy{j};
        dof=dom.ind_P(bdy); 
        out(dof,:)=P(bdy,:)-P_v; // valve pressure on the bdy
        // compute/accumulate flow on input bdys
        R=p.nodes.R(dom.nodes(dom.bdy{j}));
        Z=p.nodes.Z(dom.nodes(dom.bdy{j}));
        if (max(Z)-min(Z))==0 // horizontal bdy
          innerflow=innerflow-2*%pi*p.nodes.dr*alpha3*trapz(R.*P_over_Tmu_g(dom.bdy{j},:).*(dom.bdyDn{j}*P));
        else // vertical bdy
          innerflow=innerflow-2*%pi*R(1)*p.nodes.dz*alpha3*trapz(P_over_Tmu_g(dom.bdy{j},:).*(dom.bdyDn{j}*P));
        end 
      end
    else // steel or natural expanded graphite
      // ONLY TEMPERATURE (T)
      rho=p(sprintf('rho_%s',material));
      Cp=p(sprintf('C_p%s',material));
      dTdt=divLambdaT/rho/Cp;
    end
    // Temperature@interior, all materials
    out(dom.ind_T(int),:)=dTdt(int,:);
    // Temparature@boundary, update value for continuity of flux, all materials
    out(dom.ind_T(allbdy),:)=out(dom.ind_T(allbdy),:)-lambda_r*dnrT-lambda_z*dnzT;  
    // -cooling fluid convection, all materials
    if material <> 'duct',
      for j=dom.bdyCool
        nodes=dom.bdy{j};
        dof=dom.ind_T(nodes);
        out(dof,:)=out(dof,:)-p.h*(T(nodes,:)-p.T_f_function(t,p)); 
      end     
    end
  end // loop on layers/bricks
  coeff=1e6;
  // "coeff" plays the role of 
  // V*M_g/R/T_f*1e5 where V is the volume of the valve
  // The larger this coefficient, the faster the response of
  // the valve... and the stiffer the ODE !
  // 1e7 for a fast valve, 1e5 for a sluggish one.
  out(p.nodes.ind_P_v,:)=coeff*(innerflow-common.flowsat(t,P_v,p));
  //
  // compute final residual for DAE solver
  //
  out = out-p.mass*Xp;
  //
end

function out=trapz(x)
    out=(x(1)+x($))/2+sum(x(2:$-1));
end
 

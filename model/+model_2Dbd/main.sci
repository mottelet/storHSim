// model_2Dbd/main.sci
// 2D simulation : main script
//
// Authors: 
//
// MOTTELET S. (UTC), stephane.mottelet@utc.fr
// LETURIA M. (UTC)
// MANAI M. S. (UTC)
//
// This file is hereby licensed under the terms of the GNU GPL v3.0,
// For more information, see the COPYING file which you should have received//
// This file is hereby licensed under the terms of the GNU GPL v3.0,
// For more information, see the COPYING file which you should have received
//
// Last modified : Thu Jul 20 11:49:34 CEST 2023

path = get_absolute_file_path();
// generate libraries for functions in folders "+*"
libs = listfiles(fullfile(path,"..","+*"));
for alib=libs'
    genlib(strsplit(alib,"+")($),alib,verbose=%t)
    load(fullfile(alib,"lib"))
end

if ~exists("xlreadwritelib") then
    exec(fullfile(path,"..","xlreadwrite-1.2.4","loader.sce"))
end

param.model='model_2Dbd';

fieldsChem={
'metal','Metal name',''
'rho_smin','Empty metal hydride density','kg/m^3'
'rho_ssat','Saturation hydride density','kg/m^3'
'deltaH_a','Reaction enthalpy (absorption)','J/mol'
'deltaH_d','Reaction enthalpy (desorption)','J/mol'
'deltaS','Reaction entropy','J/(mol.K)'
'M_s','Hydride molecular weight','kg/mol'
'C_a','Kinetic constant (absorption)','1/s'
'C_d','Kinetic constant (desorption)','1/s'
'E_a','Activation energy (absorption)','max(J/mol'
'E_d', 'Activation energy (desorption)','J/mol'
'C_ps','Hydride specific heat','J/(kg.K)'
'epsilon','Hydride porosity',''
'K_mh','Hydride permeability','m^2'
'lambda_mh_r','MH thermal conductivity','W/(m.K)'
'lambda_mh_z','MH thermal conductivity','W/(m.K)'

'M_g','Gas molecular weight','kg/mol'
'C_pg','Gas specific heat','J/(kg.K)'
'C_pgne','GNE specific heat','J/(kg.K)'
'C_psteel','Steel specific heat','J/(kg.K)'
'rho_gne','GNE density','kg/m^3'
'lambda_gne_r','GNE thermal conductivity','W/(m.K)'
'lambda_gne_z','GNE thermal conductivity','W/(m.K)'
'rho_steel','Steel density','kg/m^3'
'lambda_steel_r','GNE thermal conductivity','W/(m.K)'
'lambda_steel_z','GNE thermal conductivity','W/(m.K)'
'h','Heat transfer coefficient','W/(m^2.K)'
 };

fieldsSim={'P_in','Input gas pressure','bar'
'flow','Maximum Gas mass flow rate','Kg/s'
'T_f','Temperature of cooling/heating fluid','K'
'T0','Initial temperature','K'
'rho_s0','Initial hydrure density','kg/m^3'
't_closing','Time of valve closing','s'
'tmax','Maximum time','s'
'tspan','Values of t for simulation','s'};

fieldsVisual={'z_values','z values for 1D view','m'
  'r_values','z values for 1D view','m'
  'T_limits','min and max values for T','K'
  'P_limits','min and max values for P','bar'
  'flow_limits','min and max values for flow','Kg/s'
  'rhog_limits','min and max values for rhog','Kg/m^3'
  'rhos_limits','min and max values for rhos','Kg/m^3'
  'mflow_limits','min and max values for mass flow rate','Kg/m^3/s'
  };

fieldsSolver={'RelTol','Relative tolerance for ode15s solver',''
  'dr','Space discretization step (dz=dr)','m'
  'nDof','Number of degrees of freedom',''};

fieldsGeom={'inner_radius','Canister inner radius','m'
'outer_radius','Canister outer radius','m'
'height','MH+GNE height','m'
'bot_layer','Type of bottom layer (1=MH|0=GNE)',''
'h_mh','Thickness of Hydrure layers','m'
'h_gne','Thickness of GNE layers','m'
'bdyInput','Boundaries where Hydrogen comes in (subset of {1,2,3,4})',''
'bdyCool','Boundaries where cooling occurs (subset of {1,2,3,4})',''
'bdySteel','Boundaries surrounded by steel (subset of {1,2,3,4})',''
'h_steel','Thickness of steel boundaries','m'
'duct','Modelling the duct (0=no|1=yes)',''
};

// Hydrogen parameters

param.M_g=2e-3;
param.C_pg=14890;

// Parameters values for the chosen metal

param=LaNi4.parameters(param);

// Parameters for the Natural Expanded Graphite

param.lambda_gne_r=100;
param.lambda_gne_z=10;
param.rho_gne=1;
param.C_pgne=0.67e-3;

// Parameters for the  Stainless Steel - Grade 304 (UNS S30400)

param.lambda_steel_r=30;
param.lambda_steel_z=30;
param.rho_steel=7e3;
param.C_psteel=460;

param.lambda_duct_r=10;
param.lambda_duct_z=10;

param.K_duct=1e-11;
param.h_duct=2e-3;

// Physical constants and device parameters

param.R=8.3144621; // Universal gas constant (J.mol^-1.K^-1)
param.h=1000; // Heat transfer coefficient (W.m^-2.K^-1)

// Default simulation parameters

param.t_closing=%inf;
param.tmax=180;
param.P_in=10;
param.flow=1e-5;
param.T0=293;
param.T_f=293;
param.rho_s0=param.rho_smin;

// Default visualization parameters

param.z_values=0.01;
param.r_values=[0.005 0.01 0.015 0.02];
param.T_limits=[];
param.P_limits=[];
param.flow_limits=[];
param.rhog_limits=[];
param.rhos_limits=[];
param.mflow_limits=[];
param.tspan=[];

// Solver parameters

param.RelTol=1e-12; // not greater than 1e-8 please !
param.dr=1e-3;

// Default geometry

param.inner_radius=0.004;
param.outer_radius=0.02;
param.bot_layer=1;
param.height=0.017;
param.h_gne=0.002;
param.h_mh=0.005;
param.V=%pi*param.height*(param.outer_radius^2-param.inner_radius^2); // Volume
param.bdyCool=[3 4];
param.bdyInput=1;
param.bdySteel=[4 3];
param.h_steel=0.002;
param.duct=1;

//
model_2Dbd.gui(param,fieldsGeom,fieldsChem,fieldsSim,fieldsSolver,fieldsVisual)
model_2Dbd.scenario('Constant P_in and max flow')
clear gcbo
gcbo.tag="init";
model_2Dbd.initialize
clear gcbo

// model_2Dbd/initialize.sci
// Matrices and structures (finite differences method) for the 2D axi-symmetric 
// hydrogen (de)sorption problem.
// Kronecker product trick explained in https://www.math.uci.edu/~chenlong/226/FDMcode.pdf
//
// Authors: 
//
// MOTTELET S. (UTC), stephane.mottelet@utc.fr
// LETURIA M. (UTC)
// MANAI M. S. (UTC)
//
// This file is hereby licensed under the terms of the GNU GPL v3.0,
// For more information, see the COPYING file which you should have received//
// This file is hereby licensed under the terms of the GNU GPL v3.0,
// For more information, see the COPYING file which you should have received
//
// Last modified : Thu Jul 20 11:49:34 CEST 2023

function initialize
    f=gcf();
    p=f.UserData.param;

    if exists("gcbo")
        if gcbo.tag == "init"

            findobj("tag","compute").enable=%f;
            findobj("tag","init").enable=%f;
            dr=p.dr;
            dz=dr;

            // Boundaries numbering
            //
            //     2
            //    ___
            //   |---|
            //   |---|
            // 1 |---| 3
            //   |___|
            //
            //     4
            //
            //
            // Build layers data
            //
            typeString={'gne','mh','duct'};
            layerHeight=[p.h_gne, p.h_mh];
            current_z=0;
            current_layer_type=p.bot_layer;

            i=1;
            i_gne=1;
            i_mh=1;
            while current_z - p.height < -%eps
                i=i+1;
                types{i}=typeString{current_layer_type+1};
                if ~strcmp(types{i},'gne')
                    layerHeight=p.h_gne(i_gne-floor((i_gne-1)/length(p.h_gne))*length(p.h_gne));
                    i_gne=i_gne+1;
                else
                    layerHeight=p.h_mh(i_mh-floor((i_mh-1)/length(p.h_mh))*length(p.h_mh));
                    i_mh=i_mh+1;
                end
                height=min(p.height,current_z+layerHeight)-current_z;
                next_z=current_z+height;
                rects(i,:)=[p.inner_radius p.outer_radius current_z next_z];
                // warning, last layer can be thinner than expected :
                current_z=next_z;
                current_layer_type=~current_layer_type; // toggle layer type
                last_layer=current_z - p.height >= -%eps;
            end

            zmin=0;
            zmax=p.height;
            rmin=p.inner_radius;
            if p.duct
                rmin=p.inner_radius-p.h_duct;
            end
            rmax=p.outer_radius;

            if ~isempty(p.bdySteel) && ~isempty(p.h_steel) && and(p.h_steel>0)    
                h_steel=p.h_steel;
                if length(h_steel)<length(p.bdySteel)
                    h_steel($+1:length(p.bdySteel))=h_steel($);
                end
                if or(4==p.bdySteel)
                    i=i+1;
                    rects(i,:)=[rmin p.outer_radius -h_steel(p.bdySteel==4) 0];
                    types{i}='steel';
                    zmin=-h_steel(p.bdySteel==4);
                end
                if or(2==p.bdySteel)
                    i=i+1;
                    rects(i,:)=[rmin p.outer_radius p.height p.height+h_steel(p.bdySteel==2)];
                    types{i}='steel';
                    zmax=p.height+h_steel(p.bdySteel==2);
                end
                if or(3==p.bdySteel)
                    i=i+1;
                    rects(i,:)=[p.outer_radius p.outer_radius+h_steel(p.bdySteel==3) zmin zmax];
                    types{i}='steel';
                    rmax=p.outer_radius+h_steel(p.bdySteel==3);
                end      
            end

            p.rmin=rmin;
            p.rmax=rmax;
            p.zmin=zmin;
            p.zmax=zmax;

            if p.duct
                i=i+1;
                types{i}='duct';
                rects(i,:)=[rmin p.inner_radius 0 p.height];
            end

            // main enclosing rectangle
            rects(1,:)=[rmin rmax zmin zmax];
            types{1}='none';

            try
                p=model_2Dbd.build(p,rects,types,dr,dz);
            catch
                
                messagebox(lasterror(),'modal');
                findobj("tag","init").enable=%t;
                return
            end
        end
    end

    rmin=p.rmin;
    rmax=p.rmax;
    zmin=p.zmin;
    zmax=p.zmax;
    dz=p.dr;

    rect=[rmin rmax zmin zmax];

    tabGeom=findobj(f,'string','Geometry');
    fr=findobj(tabGeom,"tag","domain");
    delete(fr.children);      
    ax=newaxes(fr);
    ax.axes_visible="on"
    ax.data_bounds()=[0 rmax zmin zmax];
    ax.isoview=%t;
    ax.margins=0.15([1 1 1 1]);
    ax.background=gcf().background;
    xstringb(rect(1)-(rect(2)-rect(1))/7,mean(rect(3:4)),'1',0,0);
    gce().font_size=3;
    xstringb(rect(2)+(rect(2)-rect(1))/7,mean(rect(3:4)),'3',0,0);
    gce().font_size=3;
    xstringb(mean(rect(1:2)),rect(4)+(rect(4)-rect(3))/10,'2',0,0);
    gce().font_size=3;
    xstringb(mean(rect(1:2)),rect(3)-(rect(4)-rect(3))/10,'4',0,0);
    gce().font_size=3;

    col.steel=[140 207 255]/255;
    col.mh=[253 126 143]/255;
    col.gne=[184 184 184]/255;
    col.duct=[0 255 0]/255;

    xrect([0 zmax rmax zmax-zmin]);
    gce().line_style=7;
    xsegs([0 0],[zmin-5*dz zmax+5*dz]);
    gce().line_style=7;

    for i=1:length(p.dom)
        xstringb(mean(p.dom{i}.rect(1:2)),mean(p.dom{i}.rect(3:4)),...
            sprintf("%s#%d",p.dom{i}.type,i),0,0);
    end

    for i=1:length(p.dom)
        domcolor=col(p.dom{i}.type);
        p.dom{i}.color=col(p.dom{i}.type);
        xfrect([1 0 0 0;0 0 0 1;-1 1 0 0;0 0 -1 1]*p.dom{i}.rect');
        gce().tag=strcat('rect',string(i));
        gce().background=addcolor(domcolor);
        p.dom{i}.visible=%t;
    end

    glue(ax.children);
    tabGeom.user_data.axes=ax;

    if exists("gcbo")
        if gcbo.tag == "init"
            p=model_2Dbd.physics(p);  
        end
    end

    f.UserData.param=p;
    common.refresh
    if ~isempty(p.Jpat)
        findobj("tag","compute").enable=%t;
    end
    findobj("tag","init").enable=%t;
    //  common.plot_results
end



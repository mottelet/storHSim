// common/build.sci
// axisymetric geometry builder (physics (i.e. matrices) is built later)
//
// Authors: 
//
// MOTTELET S. (UTC), stephane.mottelet@utc.fr
// LETURIA M. (UTC)
// MANAI M. S. (UTC)
//
// This file is hereby licensed under the terms of the GNU GPL v3.0,
// For more information, see the COPYING file which you should have received//
// This file is hereby licensed under the terms of the GNU GPL v3.0,
// For more information, see the COPYING file which you should have received
//
// Last modified : Thu Jul 20 11:49:34 CEST 2023

function p=build(p,rects,material,dr,dz)
  // rects(i,:)=[rmin rmax zmin zmax]
  // first rectangle is the main enclosing rectangle and holes are not allowed !
  for i=2:size(rects,1)
    if and(rects(i,1:2)>=rects(1,1) & rects(i,1:2)<=rects(1,2)...
        & rects(i,3:4)>=rects(1,3) & rects(i,3:4)<=rects(1,4)) // if rectangle is inside enclosing rectangle
      for j=i+1:size(rects,1)
        if rects(i,1)<rects(j,2) && rects(i,2)>rects(j,1) && rects(i,3)<rects(j,4) && rects(i,4)>rects(j,3)
          error('Rectangle #%d overlaps rectangle #%d!',i,j)
        end
      end
    else
      error('Rectangle #%d is not inside rectangle #1 !',i)
    end 
  end

  areas=(rects(:,2)-rects(:,1)).*(rects(:,4)-rects(:,3));
  if abs(sum(areas)-2*areas(1))>%eps
    warning('Rectangles do not completely fill rectangle #1 !')
  end

  N=1+round((rects(1,2)-rects(1,1))/dr); // main rectangle dims, r direction
  M=1+round((rects(1,4)-rects(1,3))/dz); // z direction
  nodes_mat=matrix(1:N*M,M,N);

  p.nNodes=N*M;  
  
  k=[1 3 1 4 2 4 2 3]; // to form successive coordinates pairs of vertices
  rz=unique(matrix(rects(:,k)',2,-1)','r'); // unique vertices

  if isfield(p,'dom')
    p.dom=null();
  end
  p.dom=cell();
  // rectangle vertices (+ non trivial ones)

  p.nodes.dr=dr;
  p.nodes.dz=dz;
  p.nodes.r=rects(1,1):dr:rects(1,2);
  p.nodes.z=rects(1,3):dz:rects(1,4);
  [p.nodes.R,p.nodes.Z]=meshgrid(p.nodes.r,p.nodes.z);
  
  for i=2:size(rects,1)
    indr=1+round(((rects(i,1):dr:rects(i,2))-rects(1,1))/dr);
    indz=1+round(((rects(i,3):dz:rects(i,4))-rects(1,3))/dz);
    Ml=length(indz);
    Nl=length(indr);
//    disp([Ml,Nl])
    dom.rect=rects(i,:); // rects(i,:)=[rmin rmax zmin zmax]
    dom.r=p.nodes.r(indr);
    dom.z=p.nodes.z(indz);

    // rectangle vertices (+ non trivial ones)
    iv=find(rz(:,1)>=rects(i,1) & rz(:,1)<=rects(i,2)...
      &  rz(:,2)>=rects(i,3) & rz(:,2)<=rects(i,4));
    k=convhull(rz(iv,1),rz(iv,2));
    vertices=rz(iv(k($:-1:1)),:);
    vertices_ind=round(1+Ml*(vertices(:,1)-rects(i,1))/dr+(vertices(:,2)-rects(i,3))/dz);
    bdy=cell(1);
    bdypos=0;
    locallbdy=[];
    jbdy=1;
    for j=1:length(k)-1 // loop on vertices
      vectdir=vertices(j+1,:)-vertices(j,:); // direction vector vertex#j->vertex#j+1
      // local nodes numbers
      locbdyj=vertices_ind(j):sign(vectdir)*[Ml;1]:vertices_ind(j+1);
      // vertical bdys at r=0 are not bdys per se because symmetry
      // condition is built into the laplacian and gradient matrices
      if or(vertices(j:j+1,1)~=0)
        locallbdy=[locallbdy locbdyj]; 
        // local nodes numbers
        bdy{jbdy}=locbdyj;
        // bdy position within enclosing rectangle
        if and(vertices(j:j+1,1)==rects(1,1))
          bdypos(jbdy)=1; // left
        elseif and(vertices(j:j+1,1)==rects(1,2))
          bdypos(jbdy)=3; // right
        elseif and(vertices(j:j+1,2)==rects(1,3))
          bdypos(jbdy)=4; // bottom
        elseif and(vertices(j:j+1,2)==rects(1,4))
          bdypos(jbdy)=2; //top
        else
          bdypos(jbdy)=0; //inner bdy
        end
        jbdy=jbdy+1;
      end
    end
    //
    domnodes=nodes_mat(indz,indr);
    dom.nodes=domnodes(:); // global node numbers of layer nodes
    dom.M=Ml;
    dom.N=Nl;
    dom.bdy=bdy;
    dom.bdypos=bdypos;
    dom.allbdy=unique(locallbdy,'keepOrder'); // local nodes numbers
    dom.int=setdiff(1:Ml*Nl,dom.allbdy); // global nodes numbers
    dom.type=material{i};
    p.dom{i-1}=dom;
  end
end

function k=convhull(x,y)
    xm=(min(x)+max(x))/2;
    ym=(min(y)+max(y))/2;
    u=x-xm;
    v=y-ym;
    th=atan(v,u);
    [_,k]=gsort(-th);
    k($+1)=k(1);
end


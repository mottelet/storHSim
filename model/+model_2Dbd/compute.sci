// model_2Dbd/compute.sci
// 2D simulation: callback function (compute and plot system states/outputs)
//
// Authors: 
//
// MOTTELET S. (UTC), stephane.mottelet@utc.fr
//
// This file is hereby licensed under the terms of the GNU GPL v3.0,
// For more information, see the COPYING file which you should have received
//
// Last modified : Thu Jul 20 11:49:34 CEST 2023

function varargout=compute(param)
  
  if argn(2)==0 // interactive call from GUI
    f=gcf();
    bt=findobj(f,'tag','compute');
    bt.Enable='off';
    stopb=findobj(f,'tag','Stop');
    stopb.Enable='on';
    // drawnow
    f.UserData.param.stop=%f;
    param=f.UserData.param;
  else // non-interactive call
    varargout(1)=param;
  end
  
  // Initial condition
  X0=zeros(param.nDof,1);
  P0=param.P_eq_value(param.T0,param.rho_s0,param);
  for i=1:length(param.dom)
    X0(param.dom{i}.ind_T)=param.T0;
    if param.dom{i}.type == 'mh'
      X0(param.dom{i}.ind_rho_s)=param.rho_s0;
      X0(param.dom{i}.ind_P)=P0;
    elseif param.dom{i}.type == 'duct' // Was completely missed for days (21/11/2017 fix) !!!!
      X0(param.dom{i}.ind_P)=P0;
    end
  end
  X0(param.nodes.ind_P_v)=P0;
  
  param.P0=P0;
  
  param.K_ref=param.K_mh; // reference permeability (for scaling of pressure boundary conditions)
  if param.duct
    param.K_ref=max(param.K_mh,param.K_duct);
  end
  
  winH=waitbar(0)
  OPT=struct();
  OPT.rtol = param.RelTol;
  OPT.jacPattern = param.Jpat+param.mass;
  OPT.calcIc="y0yp0";
  
  if isempty(param.tspan)
      tspan = linspace(0,param.tmax,1024);
  else
      tspan = param.tspan;
  end

  if argn(2)==0 // interactive call
    OPT.callback=list(common.odeStat,winH,tspan($));
  end
  
  //  Final computation
  [t,sol,info]=ida(list(model_2Dbd.rhs,param),tspan,X0,zeros(X0),options=OPT);
  
  delete(winH)
  disp(info.stats)

//   //////// scenario limite wtpct
// 
//   [t,wtpct]=compute_wtpct(sol,param);
//   param.t_closing=t(min(find(wtpct>0.25)));
//   sol=ode15s(@(t,X)model_2Dbd.rhs(t,X,param),[0 param.tmax],X0,options);
//   
//   ////////
  
  if argn(2)==0 // interactive call
    f.UserData.param.etime=info.stats.eTime;
    stopb.Enable='off';
    set(bt,'enable','on','string','Run ');
    //if ~f.UserData.param.stop
      f.UserData.param.sol=sol;
      f.UserData.param.soltspan=t;
      f.UserData.param.new=%t;
      model_2Dbd.plot_results;
      f.UserData.param.new=%f;
    //end
  else // non-interactive call
    varargout(1).sol=sol;
    varargout(1).etime=info.stats.eTime;
  end

end


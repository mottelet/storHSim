// model_2Dbd/gui.m
// 2Ds simulation, build graphical user interface (gui)
//
// Authors: 
//
// MOTTELET S. (UTC), stephane.mottelet@utc.fr
// LETURIA M. (UTC)
// MANAI M. S. (UTC)
//
// This file is hereby licensed under the terms of the GNU GPL v3.0,
// For more information, see the COPYING file which you should have received//
// This file is hereby licensed under the terms of the GNU GPL v3.0,
// For more information, see the COPYING file which you should have received
//
// Last modified : Thu Jul 20 11:49:34 CEST 2023

function gui(param,fieldsGeom,fieldsChem,fieldsSim,fieldsSolver,fieldsVisual)

    hdl=get("model_2Dbd_gui");
    if ~isempty(hdl)
        delete(hdl)
    end

    f=figure("dockable", "off" ,"infobar_visible", "off", "toolbar_visible", "off", ...
    "menubar_visible", "off", "default_axes", "off","visible","off","layout", "border",...
    "tag","model_2Dbd_gui")
    f.background=addcolor([238 238 238]/255);

    leftfr=uicontrol(f,"style","frame","constraints",createConstraints("border", "left",[300,1]),"layout","border")  
    tabParm=uitabgroup(leftfr,"constraints",createConstraints("border", "top",[300,600])); // ,"layout","border"
    OPT=list("constraints",createConstraints("border","top"),"layout","border")
    tabParm4=uitab(tabParm,'title','Credits');
    tabParm5=uitab(tabParm,'title','Solver parameters',OPT(:));
    tabParm6=uitab(tabParm,'title','Visualization',OPT(:));
    tabParm3=uitab(tabParm,'title','Simulation',OPT(:));
    tabParm2=uitab(tabParm,'title','Chemical constants',OPT(:));
    tabParm1=uitab(tabParm,'title','Geometry',OPT(:));

    leftbotfr=uicontrol(leftfr,"style","frame","constraints",createConstraints("border", "bottom",[250,40]),...
    "layout","grid","layout_options",createLayoutOptions("grid", [1,5],[0 0]))  

    uicontrol(tabParm1,"style","frame","constraints",createConstraints("border", "center",[-1,-1]),"tag","domain")  
    
    OPT=list(leftbotfr,'style','pushbutton',"constraints",createConstraints("grid"),'FontSize',11);

    uicontrol(OPT(:),'string','Load','callback','common.loadModel','tag','load')
    uicontrol(OPT(:),'string','Save','callback','common.saveModel','tag','save')
    uicontrol(OPT(:),'string','Stop','tag','Stop','callback','common.stop','enable','off',...
    "callback_type",10)
    uicontrol(OPT(:),'string','Run','callback','model_2Dbd.compute;','tag','compute','enable','off',...
    "callback_type",0)
    uicontrol(OPT(:),'string','Build','callback','model_2Dbd.initialize;','tag','init')

    tabGraph=uitabgroup(f,"constraints",createConstraints("border", "right",[800,1]));

    variables={'Temperature','Pressure','Flow','Gas density','Hydride density','Global wt%','Mass flow rate'};

    for i=7:-1:1
        tab=uitab(tabGraph,'title',variables{i},"constraints",createConstraints("border", "center"),"layout","border")
        tabg=uitabgroup(tab,"constraints",createConstraints("border", "center"));
        if variables{i} <> 'Global wt%' && variables{i} <> 'Flow'
            uitab(tabg,'title','3d view','layout','border');
            uitab(tabg,'title','2d view','layout','border');
        end
        uitab(tabg,'title','1d view');
        tabg.value=1;
    end
    tabGraph.value=1;
    //
    h=newaxes(tabParm4);
    h.background=f.background
    banner=['Solid storage of Hydrogen'
    '2D bricks axisymmetric simulation'
    ''
    'Copyright (C)'
    '2016-2023 - UTC - S. MOTTELET'
    '2016-2019 - UTC - M. LETURIA'
    '2016-2019 - AAQIUS - M. S. MANAI'
    ''
    'https://gitlab.com/mottelet/storHSim'
    ]
    xstring(0,.5,banner)
    gce().font_size=2;

    paramsGui(tabParm1,fieldsGeom,'common.setparam');
    paramsGui(tabParm2,fieldsChem,'common.setparam');
    paramsGui(tabParm3,fieldsSim,'common.setparam');
    paramsGui(tabParm5,fieldsSolver,'common.setparam');
    paramsGui(tabParm6,fieldsVisual,'common.setparam');
    //  h=get("z_values");
    //  h.Callback= h.Callback+";model_2Dbd.plot_results(""1d"")"
    //  h=get("r_values");
    //  h.Callback= h.Callback+";model_2Dbd.plot_results(""1d"")"

    set(findobj(tabParm5,'tag','nDof'),'enable','off');
    set(findobj(tabParm2,'tag','metal'),'enable','off');

    uicontrol(tabParm3,'tag','scenario','style','listbox',"constraints",createConstraints("border", "bottom"),...
    'string',['Constant P_in and max flow'],'callback','model_2Dbd.scenario');

    frvis=uicontrol(tabParm6,'style','frame',...
    "constraints",createConstraints("border", "bottom",[-1 50]),"layout","border")
    uicontrol(frvis,'tag','update_vis','style','pushbutton',"constraints",createConstraints("border", "top"),...
    'string','Update visualization','callback','model_2Dbd.plot_results');
    uicontrol(frvis,'tag','export','style','pushbutton',"constraints",createConstraints("border", "bottom"),...
    'string','Export simulation results',...
    'callback','model_2Dbd.plot_results');

    f.UserData.param=param;
    f.UserData.fields=[fieldsGeom;fieldsChem;fieldsSim;fieldsVisual;fieldsSolver];
    f.Position(3:4)=[1100 640];
    f.resize='off';
    f.visible="on";
end

function paramsGui(tab,fields,callbackstring)
    // Parameter widgets (names, tooltip and unit are defined in the 'fields' matrix)
    frpar = uicontrol(tab,"style","frame","constraints",createConstraints("border","top"),...
    "layout","grid","layout_options",createLayoutOptions("grid", [size(fields,1),3],[10 0]));

    for i=size(fields,1):-1:1
        uicontrol(frpar,'style','text','string',fields{i,3},...
        'TooltipString',fields{i,2},...
        'horizontalalignment','left',"constraints",createConstraints("grid"),'FontSize',11)
        uicontrol(frpar,'tag',fields{i,1},'style','edit',...
        'backgroundcolor',[1 1 1],...
        'horizontalalignment','right','callback',callbackstring,...
        "constraints",createConstraints("grid"),'FontSize',11)
        uicontrol(frpar,'style','text','string',fields{i,1},'TooltipString',fields{i,2},...
        'horizontalalignment','right',"constraints",createConstraints("grid"),'FontSize',11)
    end
end

function h=uitabgroup(f,varargin)
    h = uicontrol(f,"style", "tab",varargin(:));
endfunction

function h=uitab(f,p,v,varargin)
    h = uicontrol(f,"style", "frame","string",v,varargin(:));
endfunction

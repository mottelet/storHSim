// model_2dbd/plot_results.sci
//
// Authors: 
//
// MOTTELET S. (UTC), stephane.mottelet@utc.fr
// LETURIA M. (UTC)
// MANAI M. S. (UTC)
//
// This file is hereby licensed under the terms of the GNU GPL v3.0,
// For more information, see the COPYING file which you should have received//
// This file is hereby licensed under the terms of the GNU GPL v3.0,
// For more information, see the COPYING file which you should have received
//
// Last modified : Thu Jul 20 11:49:34 CEST 2023

function varargout=plot_results(dims)
  if ~exists("dims","local")
      dims = "all"
  end
  f=gcf();
  param=f.UserData.param;
  
  // Determine dof indexes for 1D plot and data export
  ind=[];
  if ~(isempty(param.z_values) || isempty(param.r_values))
    [z_values,r_values]=meshgrid(param.z_values,param.r_values);
    indz=1+round((z_values-min(param.nodes.z))/param.nodes.dz);
    indr=1+round((r_values-min(param.nodes.r))/param.nodes.dr);
    ind=(indr(:)-1)*length(param.nodes.z)+indz(:);
    ind(ind<0 | ind>param.nNodes)=[];
  end
  
  // Copy canister layout from Geometry tab
  tabGeom=findobj(f,'string','Geometry');
  tabVisual=findobj(f,'string','Visualization');

  fr=get("domainV");
  if isempty(fr)
      fr=uicontrol(tabVisual,"style","frame","constraints",createConstraints("border", "center"),"tag","domainV")    
  else
      delete(fr.children);
  end
  
  ax=newaxes(fr);
  ax.data_bounds=[0 param.rmax param.zmin-5*param.dr param.zmax+5*param.dr];
  ax.isoview=%t;
  ax.margins=0.15([1 1 1 1]);
  ax.background=gcf().background;

  copy(tabGeom.UserData.axes.children,ax);

  if ~isempty(ind)
    drawlater
    h=plot(ax,param.nodes.R(ind),param.nodes.Z(ind),'marker','o','markerfacecolor','white');
    h.line_mode="off";
    drawnow
  end

  // If solution has not been updated, then exit
  if ~isfield(param,'sol')
    return
  end
  
  t=param.soltspan;
  X=param.sol;
  
  massMH=zeros(t);
  volume=0;  
  rho_s=%nan(ones(param.nNodes,length(t)));
  rho_g=%nan(ones(param.nNodes,length(t)));
  m=%nan(ones(param.nNodes,length(t)));
  T=%nan(ones(param.nNodes,length(t)));
  P=%nan(ones(param.nNodes,length(t)));

  innerflow=0;
  for i=1:length(param.dom)
    dom=param.dom{i};
    nodes=dom.nodes;
    T(nodes,:)=X(dom.ind_T,:);
    if dom.type == 'mh'
      rho_s(nodes,:)=X(dom.ind_rho_s,:);
      P(nodes,:)=X(dom.ind_P,:);
      rho_g(nodes,:)=param.M_g*P(nodes,:)./param.R./T(nodes,:);
      m(nodes,:)=common.mass_flow(T(nodes,:),P(nodes,:),rho_s(nodes,:),param);
      r_times_rho_s=repmat(param.nodes.R(nodes),1,length(t)).*rho_s(nodes,:);
      r_times_mdot=repmat(param.nodes.R(nodes),1,length(t)).*m(nodes,:);
      massMH=massMH+trapz(trapz(matrix(r_times_rho_s,dom.M,dom.N,length(t))));
      rect=dom.rect;
      volume=volume+%pi*(rect(2)^2-rect(1)^2)*(rect(4)-rect(3));
      K=param.K_mh;
    elseif dom.type == 'duct'
      P(nodes,:)=X(dom.ind_P,:);
      rho_g(nodes,:)=param.M_g*P(nodes,:)./param.R./T(nodes,:);
      K=param.K_duct;
    end
    for j=dom.bdyInput 
      mu_g0=9.05e-6; // dynamic viscosity
      alpha3=(1e10*K*param.M_g)/(mu_g0*param.R);  
      bdy=dom.bdy{j};
      P_over_Tmu_g=P(nodes(bdy),:)./T(nodes(bdy),:).*(T(nodes(bdy),:)/293).^-0.68;
      // compute/accumulate flow on input bdys
      R=param.nodes.R(dom.nodes(bdy));
      Z=param.nodes.Z(dom.nodes(bdy));
      if (max(Z)-min(Z))==0 // horizontal bdy
        innerflow=innerflow-2*%pi*param.nodes.dr*alpha3*trapz(R.*P_over_Tmu_g.*(dom.bdyDn{j}*P(nodes,:)));
      else // vertical bdy
        innerflow=innerflow-2*%pi*R(1)*param.nodes.dz*alpha3*trapz(P_over_Tmu_g.*(dom.bdyDn{j}*P(nodes,:)));
      end 
    end
      // set(findobj(f,'tag',strcat('dom',string(i))),'visible','on');
      // set(findobj(f,'tag',strcat('rect',string(i))),'FaceColor',f.UserData.param.dom{i}.color);
  end
  // innerflow is the actual flow 
  
  P_v=X(param.nodes.ind_P_v,:);
  // Qm is the expected flow
  Qm=common.flowsat(t,P_v,param);

  massMH=2*%pi*param.nodes.dr*param.nodes.dz*massMH;
  wtpct=100*(massMH-volume*param.rho_smin)./massMH;
  
  if argn(1)>=1
    varargout(1)=struct('r',param.nodes.R(ind),'z',param.nodes.Z(ind),'t',t,...
      'T',T(ind,:),'P',P(ind,:),'rho_s',rho_s(ind,:),'rho_g',rho_g(ind,:),...
      'm',m(ind,:),'wtpct',wtpct);
  elseif ~exists("gcbo") || get(gcbo,'tag') <> 'export'
    sliders = findobj(gcf(),'Style','slider');
    sliders.enable="off"
    barh=waitbar(0,"Updating visualization: Temperature")
    plotfun(t,T,param.T_limits,ind,'Temperature','Temperature (K)',param,dims)
    waitbar(0.2,"Updating visualization: Pressure",barh);
    plotfun(t,P,param.P_limits,ind,'Pressure','Pressure (bar)',param,dims)
    // below innerflow/Qm can be exchanged as they can really differ
    // when a sluggish valve is used (see in model_2Dbd.rhs)
    plotfun(t,Qm,param.flow_limits,1,'Flow','Flow (Kg/s)',param,dims)
    //
    waitbar(0.4,"Updating visualization: Hydride density",barh);
    plotfun(t,rho_s,param.rhos_limits,ind,'Hydride density','Hydride density (Kg.m^-3)',param,dims)
    plotfun(t,wtpct,[],1,'Global wt%','Global wt%',param,dims)
    waitbar(0.6,"Updating visualization: Gas density",barh);
    plotfun(t,rho_g,param.rhog_limits,ind,'Gas density','Gas density (Kg.m^-3)',param,dims)
    waitbar(0.8,"Updating visualization: Mass flow rate",barh);
    plotfun(t,m,param.mflow_limits,ind,'Mass flow rate','Mass flow rate (Kg.m^-3.s^-1)',param,dims)
    sliders.enable="on"
    delete(barh)
  else // Export simulation results (xls)
    exts=['.xls';'.xlsx'];
    [filename, pathname, k]=uiputfile("*"+exts,'Select a spreadsheet file');
    if ~isempty(filename)
      [d,f,e]=fileparts(filename);
      if isempty(e)
        filename=filename+exts(k);
        if isfile(fullfile(pathname,filename))
            rep=messagebox("Replace existing file ?","File already exists",...
            "",["No","Yes"],"modal");
            if rep <> 2
                return
            end
        end
      end
      filename=fullfile(pathname,filename);
      xlwrite(filename,[{'R';'Z';'TIME \ DOF'};num2cell(t(:))],'','A1')
      start=write_dof(filename,T,ind,param,'T',2);
      start=write_dof(filename,P,ind,param,'P',start);
      start=write_dof(filename,P_v,[],param,'P_v',start);
      start=write_dof(filename,Qm,[],param,'Flow',start);
      start=write_dof(filename,wtpct,[],param,'wt%',start);
      start=write_dof(filename,rho_s,ind,param,'rho_s',start);
      start=write_dof(filename,rho_g,ind,param,'rho_g',start);
      [_,_]=write_dof(filename,m,ind,param,'m',start);
    end
  end
end

function [start,col]=write_dof(filename,y,ind,param,tag,start)
  col=excel_col(start);
  if isempty(ind)
    xlwrite(filename,{tag},'',col+'3');    
    xlwrite(filename,y(:),'',col+'4');
    start=start+1;
    col=excel_col(start);
  else    
    xlwrite(filename,[param.nodes.R(ind)';param.nodes.Z(ind)'],'',col+'1');
    xlwrite(filename,repmat({tag},1,length(ind)),'',col+'3');
    xlwrite(filename,y(ind,:)','',col+'4');
    start=start+length(ind);
    col=excel_col(start);
  end
end

function col=excel_col(num)
  col=ascii(uint8(num-1)/26+64)+ascii(modulo(num-1,26)+65); // Excel col conversion
  if num<=26
    col=part(col,2:$); // yank first char '@'
  end
end

function plotfun(t,y,yminmax,ind,tag,fstring,param,dims)
  tab=findobj(gcf(),'String',tag);
  tab1d=findobj(tab,'String','1d view');  
  delete(tab1d.Children);
  plot(newaxes(tab1d),t',y(ind,:)')
  if ~isempty(yminmax)
    gca().data_bounds(3:4)=yminmax;
  end
  if isempty(yminmax) // for 2D and 3D color axis
    yminmax=gca().data_bounds(3:4);
    yminmax=[min(yminmax(1),min(min(y))) max(yminmax(2),max(max(y)))];
  end
  ymin=yminmax(1);
  ymax=yminmax(2);
  
  title(sprintf('%s',fstring));
  xlabel('time (s)')

  if dims == "1d"
      return
  end

  if tag <> 'Global wt%' &&  tag <> 'Flow'
    hl=legend(legfun(param,ind),-1);
    hl.fill_mode="on"
    hl.background=hl.parent.background;
    tab2d=findobj(tab,'String','2d view');  
    tab3d=findobj(tab,'String','3d view');  
    if %t
      common.player(tab2d,param.nodes.r,param.nodes.z,t,...
        y,ymin,ymax,fstring,'pcolor2')
      common.player(tab3d,param.nodes.r,param.nodes.z,t,...
        y,ymin,ymax,fstring,'surf')
    else
//      sl=findobj(tab2d,'style','slider');
//      caxis(sl.UserData.gca,[ymin ymax])
//      sl=findobj(tab3d,'style','slider');
//      caxis(sl.UserData.gca,[ymin ymax])
    end
  end
end

function str=legfun(p,ind)
  for i=1:length(ind)
    str(i)=sprintf('r=%6.4f, z=%6.4f\n',p.nodes.R(ind(i)),p.nodes.Z(ind(i)));
  end
end

function out=trapz(x)
    if isvector(x)
        out=(x(1)+x($))/2+sum(x(2:$-1));
    elseif length(size(x))==2
        out=sum(x(2:$-1,:),1)+(x(1,:)+x($,:))/2
    elseif length(size(x))==3
        out=sum(x(2:$-1,:,:),1)+(x(1,:,:)+x($,:,:))/2
    end
    out=squeeze(out)
end

// model_2Dbd/physics.sci
// Matrices and structures (finite differences method) for the 2D axi-symmetric 
// hydrogen (de)sorption problem.
// Kronecker product trick explained in https://www.math.uci.edu/~chenlong/226/FDMcode.pdf
//
// Authors: 
//
// MOTTELET S. (UTC), stephane.mottelet@utc.fr
// LETURIA M. (UTC)
// MANAI M. S. (UTC)
//
// This file is hereby licensed under the terms of the GNU GPL v3.0,
// For more information, see the COPYING file which you should have received//
// This file is hereby licensed under the terms of the GNU GPL v3.0,
// For more information, see the COPYING file which you should have received
//
// Last modified : Thu Jul 20 11:49:34 CEST 2023

function p=physics(p)
    //
    r=p.nodes.r;
    dr=p.nodes.dr;
    z=p.nodes.z;
    dz=p.nodes.dz;
    //
    // Matrices for the Laplacian (cylindrical coordinates, homogeneous Dirichlet b.c.) in the whole domain
    // At runtime, result e.g. of of (lap.r+lap.z)*X is used only at interior nodes
    N=length(r);
    M=length(z);
    //
    // Preliminar determination of the indices of degrees of freedom (dof)
    // for each type of variable (rho_s,T,P). The chosen numbering allows
    // to minimize Jacobian bandwidth
    ind_dofs=zeros(3,p.nNodes); // T,rho_s,P : rows 1,2,3
    for i=1:length(p.dom)
        ind_dofs(1,p.dom{i}.nodes)=1; // T for all materials
        if p.dom{i}.type == 'mh'
            ind_dofs(2:3,p.dom{i}.nodes)=1; // add rho_s,P
        elseif p.dom{i}.type == 'duct'
            ind_dofs(3,p.dom{i}.nodes)=1; // add only P
        end
    end
    // nonzero terms of ind_nodes(:,j) give the dof numbers of node #j
    ind_dofs=matrix(cumsum(ind_dofs(:)),size(ind_dofs)).*ind_dofs;
    // total number of dofs
    nDof=max(ind_dofs(:,$))+1; // +1 for the valve pressure dof
    // indices of dofs in overall state vector "X"
    p.nodes.ind_T=ind_dofs(1,:); p.nodes.ind_T(p.nodes.ind_T==0)=[];
    p.nodes.ind_rho_s=ind_dofs(2,:); p.nodes.ind_rho_s(p.nodes.ind_rho_s==0)=[];
    p.nodes.ind_P=ind_dofs(3,:); p.nodes.ind_P(p.nodes.ind_P==0)=[];
    p.nodes.ind_P_v=nDof; // Valve pressure
    //
    // Initialize Jpattern
    // pattern is built as a full matrix for performance issues
    p.Jpat=zeros(nDof,nDof);
    // Initialize diagonal of mass matrix (a 0 means bdy condition enforced)
    mass_diag=ones(nDof,1);
    // Initialize effective area of hydrogen input bdy
    p.duct_area=0;
    // Build layers matrices and other data
    barh=waitbar(0,"Building physics")
    try
        for i=1:length(p.dom)
            waitbar((i-1)/length(p.dom),...
            sprintf("Building physics, domain %d/%d (%s,%d dof)",i,length(p.dom),...
            p.dom{i}.type,length(p.dom{i}.nodes)),...
            barh)
            Ml=p.dom{i}.M;
            Nl=p.dom{i}.N;

            if (Ml<3 || Nl<3)
                 error(sprintf('Domain %s#%d has no interior nodes!',p.dom{i}.type,i))
            end    

            nNodesl=Ml*Nl;
            // below nodes numbers are local to the brick
            // e.g., matrices dom.dnr and dom.dnz act on X(dom.domain)
            r=p.dom{i}.r;
            z=p.dom{i}.z;
            dr=r(2)-r(1); // discretization is supposed uniform !
            dz=z(2)-z(1); // idem
            // Matrix to compute gradient components (d/dr,d/dz)
            gradr=spdiags(repmat([-1 0 1],Nl,1),-1:1,Nl,Nl)/2/dr;
            // d/dr=0 at r=0 because of symetry !
            if r(1)==0
                gradr(1,:)=0;
            end
            gradz=spdiags(repmat([-1 0 1],Ml,1),-1:1,Ml,Ml)/2/dz;
            p.dom{i}.gradr=kron(gradr,speye(Ml,Ml));
            p.dom{i}.gradz=kron(speye(Nl,Nl),gradz);
            // Matrix to compute normal derivative (outward normal) components
            dnz=sparse([[1 1 1 Ml Ml Ml];[1 2 3 Ml-2 Ml-1 Ml]]',[3 -4 1 1 -4 3]/2/dz);
            dnr=sparse([[1 1 1 Nl Nl Nl];[1 2 3 Nl-2 Nl-1 Nl]]',[3 -4 1 1 -4 3]/2/dr);
            dnr=kron(dnr,speye(Ml,Ml));
            dnz=kron(speye(Nl,Nl),dnz);
            // For jacobian pattern initialization,
            // take into account boundary condition "value" which is set at
            // boundary nodes
            Lap=full(dnr+dnz); // YES ! Initial Jacobian pattern for subdomain
            p.dom{i}.dnr=dnr(p.dom{i}.allbdy,:); // keep only normal derivatives on the boundary !
            p.dom{i}.dnz=dnz(p.dom{i}.allbdy,:); // idem
            // Laplacian w.r.t. r
            Lr=spdiags(repmat([1 -2 1]/dr^2,Nl,1),-1:1, Nl, Nl)...
            +diag(1./r)*spdiags(repmat([-1 0 1]/2/dr,Nl,1),-1:1, Nl, Nl);
            // Laplacian on the boundary
            if r(1)==0 // special case when inner_radius==0
                Lr(1,1:2)=[-4 4]/dr^2;
            else // only needed to compute dT/dt on the boundary for lumped pressure model
                Lr(1,1:3)=[1 -2 1]/dr^2+1/r(1)*[-3 4 -1]/2/dr; 
            end
            Lr(Nl,Nl-2:Nl)=[1 -2 1]/dr^2+1/r(Nl)*[1 -4 3]/2/dr;
            // Laplacian w.r.t. z
            Lz=spdiags(repmat([1 -2 1]/dz^2,Ml,1), -1:1, Ml, Ml);
            // Laplacian on the boundary (only needed to compute dT/dt on the boundary for lumped pressure model)
            Lz(1,1:3)=[1 -2 1]/dz^2; 
            Lz(Ml,Ml-2:Ml)=[1 -2 1]/dz^2; 
            p.dom{i}.lapr=kron(Lr,speye(Ml,Ml));
            p.dom{i}.lapz=kron(speye(Nl,Nl),Lz);
            // Update Jacobian pattern
            Lap=Lap+p.dom{i}.lapr+p.dom{i}.lapz;
            // indices of T dofs
            p.dom{i}.ind_T=ind_dofs(1,p.dom{i}.nodes);
            if p.dom{i}.type == 'mh'
                // indices of rho_s and P dofs
                p.dom{i}.ind_rho_s=ind_dofs(2,p.dom{i}.nodes);
                p.dom{i}.ind_P=ind_dofs(3,p.dom{i}.nodes);
                // bdy conditions enforced for P only for MH layers
                mass_diag(p.dom{i}.ind_P(p.dom{i}.allbdy))=0;
                // Update Jacobian pattern (legacy reordered rho_s,T,P)
                indg=[p.dom{i}.ind_rho_s p.dom{i}.ind_T p.dom{i}.ind_P];  
                I=eye(nNodesl,nNodesl);
                p.Jpat(indg,indg)=p.Jpat(indg,indg)+...
                [I, I, I
                I, Lap, I
                I, Lap, Lap];
            elseif p.dom{i}.type == 'duct'
                // indices of P dofs
                p.dom{i}.ind_P=ind_dofs(3,p.dom{i}.nodes);
                // bdy conditions enforced for P
                mass_diag(p.dom{i}.ind_P(p.dom{i}.allbdy))=0;
                // Update Jacobian pattern (legacy reordered T,P)
                indg=[p.dom{i}.ind_T p.dom{i}.ind_P];  
                I=eye(nNodesl,nNodesl);
                p.Jpat(indg,indg)=p.Jpat(indg,indg)+...
                [Lap, I
                Lap, Lap];
            else
                // Update Jacobian pattern
                indg=p.dom{i}.ind_T;
                p.Jpat(indg,indg)=p.Jpat(indg,indg)+Lap;
            end
            // bdy conditions always enforced for T
            mass_diag(p.dom{i}.ind_T(p.dom{i}.allbdy))=0;
            // Determination of layer boundaries where 
            // cooling and/or Hydrogen input occurs
            p.dom{i}.bdyInput=[];
            p.dom{i}.bdyCool=[];
            p.dom{i}.bdyDn={};

            for j=1:length(p.dom{i}.bdy)
                if or(p.dom{i}.bdypos(j)==p.bdyInput) && (p.dom{i}.type=='mh' || p.dom{i}.type=="duct")
                    p.dom{i}.bdyInput=[p.dom{i}.bdyInput j];
                    R=p.nodes.R(p.dom{i}.nodes(p.dom{i}.bdy{j}));
                    Z=p.nodes.Z(p.dom{i}.nodes(p.dom{i}.bdy{j}));
                    // update p.duct_area
                    if (max(Z)-min(Z))==0 // horizontal bdy
                        p.duct_area=p.duct_area+pi*(max(R)^2-min(R)^2);
                        p.dom{i}.bdyDn{j}=dnz(p.dom{i}.bdy{j},:); // normal derivative on this specific bdy
                        //flow=flow-2*pi*p.nodes.dr*alpha3*trapz(R.*P_over_Tmu_g(dom.bdy{j},:).*(dom.bdyDn{j}*P));
                    else // vertical bdy
                        p.duct_area=p.duct_area+2*%pi*R(1)*(max(Z)-min(Z));
                        p.dom{i}.bdyDn{j}=dnr(p.dom{i}.bdy{j},:); // normal derivative on this specific bdy
                        //flow=flow-2*pi*R(1)*p.nodes.dz*alpha3*trapz(P_over_Tmu_g(dom.bdy{j},:).*(dom.bdyDn{j}*P));
                    end
                    if ~isempty(p.dom{i}.bdyDn)
                        indg=p.dom{i}.ind_P(find(sum(abs(p.dom{i}.bdyDn{j}),1)));
                        p.Jpat(p.nodes.ind_P_v,indg)=1;
                    end
                    p.Jpat(p.nodes.ind_P_v,p.dom{i}.ind_T(p.dom{i}.bdy{j}))=1;
                    // Jac update for bdy condition
                    dof=p.dom{i}.ind_P(p.dom{i}.bdy{j});          
                    p.Jpat(dof,p.nodes.ind_P_v)=1;
                end
                if or(p.dom{i}.bdypos(j)==p.bdyCool)
                    p.dom{i}.bdyCool=[p.dom{i}.bdyCool j];
                end
            end
        end // loop on p.dom{}
    catch
        messagebox(lasterror(),'modal');
        delete(barh)
        p.Jpat=[];
        return
    end

    if p.duct_area==0 // incoherent position of input bdy
        f=messagebox('Incoherent position of input bdy ! Try bdyInput=1','modal');
        p.Jpat=[];
        return
    end
    waitbar(1,"Store Jacobian pattern",barh);

    // Jpattern update for valve pressure dof
    p.Jpat(nDof,nDof)=1;
    p.nDof=nDof;
    // mass matrix
    p.mass=spdiags(mass_diag,0,nDof,nDof);  
    p.Jpat=sparse(p.Jpat);
    delete(barh)
end

function a=spdiags(x,d,varargin)
    n=size(x,1);
    a=spzeros(n,n)
    k=1;
    for j=d(:)'
        if j<=0
            ind =  1-j:$;
        else
            ind = 1:$-j
        end
        a = a+diag(sparse(x(:,k))(ind),j);
        k = k+1;
    end
end



// LaNi4/parameters.sci
//
// Authors: 
//
// MOTTELET S. (UTC), stephane.mottelet@utc.fr
// LETURIA M. (UTC)
// MANAI M. S. (UTC)
//
// This file is hereby licensed under the terms of the GNU GPL v3.0,
// For more information, see the COPYING file which you should have received//
// This file is hereby licensed under the terms of the GNU GPL v3.0,
// For more information, see the COPYING file which you should have received
//
// Last modified : Thu Jul 20 11:49:34 CEST 2023

function param=parameters(param)
  // Parameters values for the LaNi4.8Al0.2-Hydrogen system
  param.metal='LaNi4';
  param.rho_smin=8300;
  param.rho_ssat=8354;
  param.M_s=426e-3;
  param.C_ps=355;
  param.deltaH_a=-32600;
  param.deltaH_d=-32600;
  param.deltaS=-104.5;
  param.C_a=59.187;
  param.C_d=9.57;
  param.E_a=21179.6;
  param.E_d=16420;
  param.epsilon=0.5;
  param.K_mh=1e-14;
  param.lambda_mh_r=1.32;
  param.lambda_mh_z=1.32;

  // Default functions i.e. without particular PCT curve
  param.P_eq_value=common.P_eq_value;
  param.rho_s_bounds=common.rho_s_bounds;
end
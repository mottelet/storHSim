# xlreadwrite 

This Scilab toolbox allows to read and write Excel BIFF8 .xls files and .xslx files in a transparent way, with the same syntax as the Matlab functions xlsread and xlswrite. It is based on the Apache POI Java library.
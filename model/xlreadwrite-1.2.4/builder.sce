// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
//
//   Copyright (C) 2020 - Stéphane MOTTELET
//
// This file is hereby licensed under the terms of the GNU GPL v2.0.
// For more information, see the COPYING file which you should have received
// along with this program.

mode(-1);
function builder_main()

  TOOLBOX_NAME  = "xlreadwrite";
  TOOLBOX_TITLE = "xlreadwrite - Toolbox for reading and writing xls and xlsx files";
  toolbox_dir   = get_absolute_file_path("builder.sce");

  // Check Scilab's version
  // =============================================================================

  try
	  v = getversion("scilab");
  catch
	  error(gettext("Scilab 6 or more is required."));
  end

  // Check development_tools module avaibility
  // =============================================================================

  if ~with_module('development_tools') then
    error(msprintf(gettext("%s module not installed."),'development_tools'));
  end

  // Action
  // =============================================================================

  tbx_builder_macros(toolbox_dir);
  tbx_builder_help(toolbox_dir);
  tbx_build_loader(toolbox_dir);
  tbx_build_cleaner(toolbox_dir);

endfunction 
// =============================================================================
builder_main();
clear builder_main;
// =============================================================================

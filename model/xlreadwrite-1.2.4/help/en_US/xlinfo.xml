<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2018 - Stéphane MOTTELET
 *
 * This file is hereby licensed under the terms of the GNU GPL v2.0,
 * For more information, see the COPYING file which you should have received
 * along with this program.
 *
 -->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" xmlns:scilab="http://www.scilab.org" xml:lang="en" xml:id="xlinfo">
    <refnamediv>
        <refname>xlinfo</refname>
        <refpurpose>Test if file is an Excel workbook file and get info</refpurpose>
    </refnamediv>
    <refsynopsisdiv>
        <title>Syntax</title>
        <synopsis>[status,sheets,wbformat]=xlinfo(file_path)</synopsis>
          </refsynopsisdiv>
    <refsection>
        <title>Arguments</title>
        <variablelist>
          <varlistentry>
              <term>file_path</term>
              <listitem>
                  <para>a character string giving the path of the file to be checked
                  </para>
              </listitem>
          </varlistentry>
          <varlistentry>
              <term>status</term>
              <listitem>
                  <para>A boolean with <literal>%t</literal> value if file is a valid Excel file or <literal>%f</literal> if not.
                  </para>
              </listitem>
          </varlistentry>
          <varlistentry>
              <term>sheets</term>
              <listitem>
                  <para>a vector of strings containing the sheets names.
                  </para>
              </listitem>
          </varlistentry>
          <varlistentry>
              <term>wbformat</term>
              <listitem>
                  <para>a character string giving the format of the Excel file, <literal>'xlOpenXMLWorkbook'</literal> for
                  <literal>EXCEL2007</literal> files or <literal>'xlWorkbookNormal'</literal> for <literal>EXCEL97</literal> files.
                  </para>
              </listitem>
          </varlistentry>
        </variablelist>
    </refsection>

    <refsection>
        <title>Examples</title>
        <programlisting role="example"><![CDATA[
filename = fullfile(SCI,'modules/spreadsheet/demos/xls/t1.xls');
[status,sheets,wbformat] = xlinfo(filename)

filename = fullpath(atomsGetLoadedPath("xlreadwrite")+'/demos/xlsx/Testbig.xlsx');
[status,sheets,wbformat] = xlinfo(filename)
 ]]></programlisting>
    </refsection>
    <refsection role="see also">
        <title>See also</title>
        <simplelist type="inline">
          <member>
              <link linkend="xlwrite">xlwrite</link>
          </member>
          <member>
              <link linkend="xlinfo">xlinfo</link>
          </member>
          <member>
              <link linkend="xls_open">xls_open</link>
          </member>
          <member>
              <link linkend="xls_open">xls_read</link>
          </member>
            <member>
                <link linkend="readxls">readxls</link>
            </member>
        </simplelist>
    </refsection>
    <refsection>
        <title>Bibliography</title>
        <para>This function is based on POI library (<ulink url="http://poi.apache.org/">http://poi.apache.org/</ulink>).
        </para>
    </refsection>
</refentry>


<?xml version="1.0" encoding="UTF-8"?>
<!--
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2018 - Stéphane MOTTELET
 *
 * This file is hereby licensed under the terms of the GNU GPL v2.0,
 * For more information, see the COPYING file which you should have received
 * along with this program.
 *
 -->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" xmlns:scilab="http://www.scilab.org" xml:lang="en" xml:id="xlread">
    <refnamediv>
        <refname>xlread</refname>
        <refpurpose>Read Microsoft Excel spreadsheet file using Java</refpurpose>
    </refnamediv>
    <refsynopsisdiv>
        <title>Syntax</title>
        <synopsis>
        value = xlread(file_path)
        value = xlread(file_path,sheet)
        value = xlread(file_path,range)
        value = xlread(file_path,sheet,range)
        [value,text,data] = xlread(...)
        [value,text,data,custom] = xlread(file_path,sheet,range,processFun)
      </synopsis>
    </refsynopsisdiv>
    <refsection>
        <title>Arguments</title>
        <variablelist>
          <varlistentry>
              <term>file_path</term>
              <listitem>
                  <para>a character string giving the path of the Excel file.
                  </para>
              </listitem>
          </varlistentry>
          <varlistentry>
              <term>sheet</term>
              <listitem>
                  <para>an integer giving the sheet position or a character string giving the sheet name.
                  </para>
              </listitem>
          </varlistentry>
          <varlistentry>
              <term>range</term>
              <listitem>
                  <para>a character string giving the rectangular range to be read in the sheet using Excel syntax.
                  </para>
              </listitem>
          </varlistentry>
          <varlistentry>
              <term>processFun</term>
              <listitem>
                  <para>Scilab function used to process the sheet data.
                  </para>
              </listitem>
          </varlistentry>
              <varlistentry>
                <term>value</term>
                <listitem>
                    <para>a matrix of numbers containing the numerical data found in the sheet at specfied range. The cells
                        without numerical data are represented by <literal>NaN</literal> values. The value of cells with date type is the corresponding Excel serial date number (origin is 1900). To convert to a Scilab serial date number (day 1 is 1-jan-0000) add 693960. The formated date string is in the <term>text</term> output.
                    </para>
                </listitem>
            </varlistentry>
            <varlistentry>
                <term>text</term>
                <listitem>
                    <para>a matrix of strings containing the textual data found in the sheet at specfied range. The cells
                        without text data are represented by empty strings. The string for cells with date type is the original formated date string. The Excel serial date number is in the <term>value</term> output.
                    </para>
                </listitem>
            </varlistentry>
            <varlistentry>
                <term>data</term>
                <listitem>
                    <para>a cell array containing the raw data found in the sheet at specfied range. Empty cells
                        are represented by <literal>NaN</literal> values.
                    </para>
                </listitem>
            </varlistentry>
            <varlistentry>
                <term>custom</term>
                <listitem>
                    <para>contains the eventual second output from <term>processFun</term>
                    </para>
                </listitem>
            </varlistentry>
        </variablelist>
    </refsection>
    <refsection>
        <title>Description</title>
        <para>
            This function reads a given sheet of an Excel workbook by eventually specifying the range to be read using the Excel syntax
            e.g. <literal>A1:B3</literal>. It returns the numerical data and the strings contained in the sheet cells.
        </para>
        <para>
          When the <term>sheet</term> argument is not given, then the first sheet of the workbook is read. In this case, the <term>range</term> argument must include a colon, otherwise it will be interpreted as a sheet name.
        </para>
        <para>
          When the <term>range</term> argument is not given, then the whole sheet is read and the <term>value,text</term> matrices tightly enclose the region where data of the corresponding type is found. When no data of a given type is found, the corresponding matrix is empty.
          If the resulting matrices have a non-homogeneous type, the corresponding cells are set to <literal>%NaN</literal> in <term>value</term> or the empty string <literal>""</literal> in <term>text</term>.
        </para>
        <para>
          Otherwise it can be of the form <literal>A1</literal> to read a single cell, <literal>A1:B3</literal>,  <literal>A1:</literal>, <literal>:B3</literal>, where the two latter specify the start or the end cell of the range. In that case (and when not empty) <term>value,text,data</term> have the same size as the given <term>range</term> and type non-homogeneous matrices are padded as described before.
        </para>
        <para>
          When the <term>processFun</term> parameter is given, the function is called with a single argument <literal>Data</literal> where
          <literal>Data.Value</literal> contains the raw data of the sheet, <literal>Data.Count</literal> is the number of cells and <literal>Data.WorkSheet</literal> is the sheet as a Java object. The first output of <literal>processFun</literal> replaces the raw sheet data before further analysis and the eventual second output is returned in the <term>custom</term> output argument.
        </para>
        <para>
            <warning>
                Only <literal>.xls</literal> and <literal>.xlsx</literal> files are handled (EXCEL97 xlWorkbookNormal and EXCEL2007 xlOpenXMLWorkbook). Binary <literal>.xlsb</literal> files are not supported.
            </warning>
        </para>
    </refsection>
    <refsection>
        <title>Examples</title>
        <programlisting role="example"><![CDATA[
// read first sheet
filename = fullfile(SCI,'modules/spreadsheet/demos/xls/Test1.xls');
[numbers,text,raw] = xlread(filename)

[numbers,text,raw] = xlread(filename,"A1:D4")

// read specified range of second sheet of xlsx file
filename = fullpath(atomsGetLoadedPath("xlreadwrite")+'/demos/xlsx/Testbig.xlsx');
numbers = xlread(filename,2,"B3:D6")

// read specified range of sheet named "Sheet 2" of xlsx file
numbers = xlread(filename,'Sheet 2','B4:D14')

// apply function to data
function Data = convert(Data)
    for k = 1:Data.Count
        Data.Value{k} = floor(100*Data.Value{k});
    end
end

numbers = xlread(filename,'Sheet 2','B4:D14',convert)
 ]]></programlisting>

    </refsection>
    <refsection role="see also">
        <title>See also</title>
        <simplelist type="inline">
          <member>
              <link linkend="xlwrite">xlwrite</link>
          </member>
          <member>
              <link linkend="xlinfo">xlinfo</link>
          </member>
          <member>
              <link linkend="xls_open">xls_open</link>
          </member>
          <member>
              <link linkend="xls_open">xls_read</link>
          </member>
            <member>
                <link linkend="readxls">readxls</link>
            </member>
        </simplelist>
    </refsection>
    <refsection>
        <title>Bibliography</title>
        <para>This function is based on POI library (<ulink url="http://poi.apache.org/">http://poi.apache.org/</ulink>).
        </para>
    </refsection>
</refentry>


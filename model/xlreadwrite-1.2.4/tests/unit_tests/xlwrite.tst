// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
//
//   Copyright (C) 2020 - Stéphane MOTTELET
//
// This file is hereby licensed under the terms of the GNU GPL v2.0.
// For more information, see the COPYING file which you should have received
// along with this program.
//
// <-- NO CHECK REF -->

// Generate some data
origData1 = {'A Number','Boolean Data','Empty Cells','Strings'
    1, %f, [], 'String Text'
    5, %f, [], 'Another very descriptive text'
    -6.26, %f, 'This should have been an empty cell but I made an error' 'This is text'
    1e8, %t, [], 'Last cell with text'
    1e3, %f, %nan, %nan
    1e2, %t, [], 'test'};

origData2 = eye(10,10);

xlsFile = fullfile(TMPDIR,"test.xls");
xlsxFile = fullfile(TMPDIR,"test.xlsx");

// Generate XLS file
xlwrite(xlsFile, origData1, "Sheet 1", "B3");
xlwrite(xlsFile, origData2, "Sheet 2");

// Generate XLSX file
xlwrite(xlsxFile, origData1, "Sheet 1", "B3");
xlwrite(xlsxFile, origData2, "Sheet 2");

// Verify XLS file
[status,sheets,wbformat]=xlinfo(xlsFile);
assert_checktrue(status);
assert_checkequal(sheets,["Sheet 1"; "Sheet 2"])
assert_checkequal(wbformat,"xlWorkbookNormal")
values = xlread(xlsFile,"Sheet 1","B4:B9");
assert_checkequal(values,cell2mat(origData1(2:7,1)));
[values,text,raw] = xlread(xlsFile,"Sheet 1","C4:C9");
assert_checkequal(cell2mat(raw),cell2mat(origData1(2:7,2)));
[values,text] = xlread(xlsFile,"Sheet 1","B3:E3");
assert_checkequal(strcat(text),cell2mat(origData1(1,:)));
values = xlread(xlsFile,"Sheet 2");
assert_checkequal(values,eye(10,10))

// Verify XLSX file
[status,sheets,wbformat]=xlinfo(xlsxFile);
assert_checktrue(status);
assert_checkequal(sheets,["Sheet 1"; "Sheet 2"])
assert_checkequal(wbformat,"xlOpenXMLWorkbook")
values = xlread(xlsxFile,"Sheet 1","B4:B9");
assert_checkequal(values,cell2mat(origData1(2:7,1)));
[values,text,raw] = xlread(xlsxFile,"Sheet 1","C4:C9");
assert_checkequal(cell2mat(raw),cell2mat(origData1(2:7,2)));
[values,text] = xlread(xlsFile,"Sheet 1","B3:E3");
assert_checkequal(strcat(text),cell2mat(origData1(1,:)));
values = xlread(xlsxFile,"Sheet 2");
assert_checkequal(values,eye(10,10))

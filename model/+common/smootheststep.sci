// Authors: 
//
// MOTTELET S. (UTC), stephane.mottelet@utc.fr
// LETURIA M. (UTC)
// MANAI M. S. (UTC)
//
// This file is hereby licensed under the terms of the GNU GPL v3.0,
// For more information, see the COPYING file which you should have received//
// This file is hereby licensed under the terms of the GNU GPL v3.0,
// For more information, see the COPYING file which you should have received
//
// Last modified : Thu Jul 20 11:49:34 CEST 2023

function s=smootheststep(v)
  x=max(min(1,v),0);
  s=(((-20*x+70).*x-84).*x+35).*x.^4;
end
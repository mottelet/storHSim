// scenario.sci
// 2D simulation : callback setting parameter values 
//
// Authors: 
//
// MOTTELET S. (UTC), stephane.mottelet@utc.fr
// LETURIA M. (UTC)
// MANAI M. S. (UTC)
//
// This file is hereby licensed under the terms of the GNU GPL v3.0,
// For more information, see the COPYING file which you should have received//
// This file is hereby licensed under the terms of the GNU GPL v3.0,
// For more information, see the COPYING file which you should have received
//
// Last modified : Thu Jul 20 11:49:34 CEST 2023

function setparam()
  param=gcf().UserData.param;
  h=gcbo();
  value=h.String;
  if ~isempty(value)
    value=evstr(sprintf('[%s]',value));
  end
  if or(param(h.Tag)~=value)
    param(h.Tag)=value;
    gcf().UserData.param=param;
    //refresh()
  end
end

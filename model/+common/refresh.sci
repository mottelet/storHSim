// common/refresh.m
// 2D simulation : refresh parameter values in the gui
//
// Authors: 
//
// MOTTELET S. (UTC), stephane.mottelet@utc.fr
// LETURIA M. (UTC)
// MANAI M. S. (UTC)
//
// This file is hereby licensed under the terms of the GNU GPL v3.0,
// For more information, see the COPYING file which you should have received//
// This file is hereby licensed under the terms of the GNU GPL v3.0,
// For more information, see the COPYING file which you should have received
//
// Last modified : Thu Jul 20 11:49:34 CEST 2023

function refresh()
  f=gcf();
  param=f.UserData.param;
  fields=f.UserData.fields;
  for i=1:size(fields,1)
    try
    h=get(fields{i,1});
    value=param(fields{i,1});
    if isnan(value)
      set(h,'enable','off');
    else
      set(h,'string',strcat(string(value)," "));
    end
    catch
    end
  end
  h=get('scenario');
  h.Value=find(h.String==param.scenario);
end

// common/saveModel.sci
// Save parameters values and eventually simulation results
//
// Authors: 
// MOTTELET S. (UTC), stephane.mottelet@utc.fr
// LETURIA M. (UTC)
// MANAI M. S. (UTC)
//
// This file is hereby licensed under the terms of the GNU GPL v3.0,
// For more information, see the COPYING file which you should have received// 
// Last modified : 30-Aug-2023

function saveModel
  [filename, pathname]=uiputfile("*.sod","","Select a model file");
  f=gcf();
  p=f.user_data.param;
  if ~isempty(filename)
      save(fullfile(pathname,filename),"p")
  end
end

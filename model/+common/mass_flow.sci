// Authors: 
//
// MOTTELET S. (UTC), stephane.mottelet@utc.fr
// LETURIA M. (UTC)
// MANAI M. S. (UTC)
//
// This file is hereby licensed under the terms of the GNU GPL v3.0,
// For more information, see the COPYING file which you should have received//
// This file is hereby licensed under the terms of the GNU GPL v3.0,
// For more information, see the COPYING file which you should have received
//
// Last modified : Thu Jul 20 11:49:34 CEST 2023

function [m,source]=mass_flow(T,P,rho_s,p)
  // P_eq formula in function of wt// or H/M atomic ratio

  [P_eq_abs,P_eq_des]=p.P_eq_value(T,rho_s,p);
  [rho_smin,rho_smax]=p.rho_s_bounds(T,P,p);
  // Kinetic term (Kg.m-^3.s^-1)

  m_a=p.C_a*exp(-p.E_a./(p.R*T)).*max(0,log(max(0,P)./P_eq_abs)).*max(0,rho_smax-rho_s);
  m_d=p.C_d*exp(-p.E_d./(p.R*T)).*min(0,(P-P_eq_des)./P_eq_des).*max(0,rho_s-rho_smin);
  m=m_a+m_d; // absorption or desorption can occur
  source  = -m.*T*(p.C_pg-p.C_ps)-(m_a*p.deltaH_a+m_d*p.deltaH_d)/p.M_g;

end
// rho_s_bounds.sci
// Compute equilibrium pressure
//
// Authors: 
//
// MOTTELET S. (UTC), stephane.mottelet@utc.fr
// LETURIA M. (UTC)
// MANAI M. S. (UTC)
//
// This file is hereby licensed under the terms of the GNU GPL v3.0,
// For more information, see the COPYING file which you should have received//
// This file is hereby licensed under the terms of the GNU GPL v3.0,
// For more information, see the COPYING file which you should have received
//
// Last modified : Thu Jul 20 11:49:34 CEST 2023

function [rho_smin,rho_smax]=rho_s_bounds(T,P,param)
  // Basic limits when PCT curve is supposed flat
  rho_smin=param.rho_smin;
  rho_smax=param.rho_ssat;
end
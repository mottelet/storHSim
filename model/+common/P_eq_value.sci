// P_eq_value.sci
// Compute equilibrium pressure
//
// Authors: 
//
// MOTTELET S. (UTC), stephane.mottelet@utc.fr
// LETURIA M. (UTC)
// MANAI M. S. (UTC)
//
// This file is hereby licensed under the terms of the GNU GPL v3.0,
// For more information, see the COPYING file which you should have received//
// This file is hereby licensed under the terms of the GNU GPL v3.0,
// For more information, see the COPYING file which you should have received
//
// Last modified : Thu Jul 20 11:49:34 CEST 2023

function [P_eq_abs,P_eq_des]=P_eq_value(T,rho_s,p)
  // Default behavior without hysteresis nor particular PCT curve
  P_eq_abs=exp(p.deltaH_a/p.R./T-p.deltaS/p.R);
  P_eq_des=exp(p.deltaH_d/p.R./T-p.deltaS/p.R);
end
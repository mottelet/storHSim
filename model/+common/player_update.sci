// common/player_update.sci
// update plot data
//
// Authors: 
//
// MOTTELET S. (UTC), stephane.mottelet@utc.fr
// LETURIA M. (UTC)
// MANAI M. S. (UTC)
//
// This file is hereby licensed under the terms of the GNU GPL v3.0,
// For more information, see the COPYING file which you should have received//
// This file is hereby licensed under the terms of the GNU GPL v3.0,
// For more information, see the COPYING file which you should have received
//
// Last modified : Thu Jul 20 11:49:34 CEST 2023

function player_update(sl)
    if argn(2) == 0
        sl = gcbo;
    end
    k=round(sl.Value);
    if isfield(sl.user_data,"pValue") && sl.user_data.pValue == k
        return
    end
    sl.user_data.pValue = k;
    title(sl.user_data.gca,sprintf('%s, t=%.1f',sl.user_data.fstring,sl.user_data.t(k)));
    if sl.user_data.style == 'pcolor2'
        sl.user_data.h.data(:,3) = sl.user_data.f(:,k);
    elseif sl.user_data.style=="surf"
        h = sl.user_data.h;
        c=round((sl.user_data.f(:,k)-sl.user_data.fmin)/(sl.user_data.fmax-sl.user_data.fmin)*(sl.user_data.NC-1)+sl.user_data.START);
        h.data.color=c(h.user_data);
    end
end   

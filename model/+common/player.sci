// common/player.m
// 2D simulation : this function creates the gui allowing to animate the
// simulation results
// Authors: 
//
// MOTTELET S. (UTC), stephane.mottelet@utc.fr
// LETURIA M. (UTC)
// MANAI M. S. (UTC)
//
// This file is hereby licensed under the terms of the GNU GPL v3.0,
// For more information, see the COPYING file which you should have received//
// This file is hereby licensed under the terms of the GNU GPL v3.0,
// For more information, see the COPYING file which you should have received
//
// Last modified : Thu Jul 20 11:49:34 CEST 2023

function player(tab,r,z,t,f,fmin,fmax,fstring,style)
  if ~isempty(findobj(tab,'type','Axes'))
    sl=findobj(tab,'style','slider');
    data=sl.user_data;
    value=sl.Value;
    set(sl,'Max',length(t));
    value=min(value,sl.Max);
    sl.Value=value;
    frtop=data.gca.parent;
    delete(frtop.children);
    ax=newaxes(frtop);
    data.gca=ax;
    data.r=r;
    data.z=z;
    data.t=t;
    data.f=f;
    data.fmin=fmin;
    data.fmax=fmax;
    h=player_init_plot(r,z,t,f,fmin,fmax,fstring,style,round(value),data);
    data.h=h;
    data.pValue=-1;
    sl.user_data=data;
  else
    data.NC=512;
    data.START=1024;
    frtop = uicontrol(tab,"style","frame","constraints",createConstraints("border","top",[0,500]))  
    ax=newaxes(frtop);
    data.r=r;
    data.z=z;
    data.t=t;
    data.f=f;
    data.fstring=fstring;
    data.style=style;
    data.gca=ax;
    data.fmin=fmin;
    data.fmax=fmax;
    h=player_init_plot(r,z,t,f,fmin,fmax,fstring,style,1,data);
    sl=uicontrol(tab,'Style', 'slider',"constraints",createConstraints("border","bottom",[0,40]),...
      'Min',1,'Max',length(t),'sliderstep',[1 20],...
      'snaptoticks','on',...
      'callback','player_update','callback_type',2);
    data.h=h;
    sl.user_data=data;
    sl.Value=1;
  end
  player_update(sl)
end

function h=player_init_plot(r,z,t,f,fmin,fmax,fstring,style,ini,data)
    fig=gcf();
    p=fig.user_data.param;
    NC=data.NC;
    START=data.START;
    cmap=[jetcolormap(NC);0.5 0.5 0.5];
    gray=START+NC;
    fig.color_map(START:START+NC,:)=cmap;
    if style=="pcolor2"
        Sgrayplot(z,r,matrix(f(:,ini),length(z),length(r)),zminmax=[fmin fmax],colminmax=[START START+NC-1]);
        h=gce().children;
        h.data(:,1:2)=h.data(:,[2 1]);
        for i=1:length(p.dom)
            xrect([1 0 0 0;0 0 0 1;-1 1 0 0;0 0 -1 1]*p.dom{i}.rect');
        end
        xlabel('r (m)')
        ylabel('z (m)')
        isoview on
        gca().data_bounds(1)=0;
        colorbar(fmin,fmax,[START START+NC-1]);
    elseif style == "surf"
        N=length(p.nodes.r);
        M=length(p.nodes.z);
        th=linspace(-%pi/4,%pi,32);
        ax=gca();
        delete(ax.Children)
        drawlater
        h=[];
        for i=1:length(p.dom)
            inside=p.dom{i}.nodes(p.dom{i}.int);
            if ~or(isnan(f(inside)))
                bdy=p.dom{i}.nodes(p.dom{i}.allbdy); // global bdy nodes numbers
                bdy=[bdy;bdy(1)]
                fbdy=repmat(bdy,1,32);
                rbdy=p.nodes.R(bdy);
                zbdy=repmat(p.nodes.Z(bdy),1,32);
                xbdy=rbdy*cos(th);
                ybdy=rbdy*sin(th);
                domain=p.dom{i}.nodes;
                R=matrix(p.nodes.R(domain),p.dom{i}.M,p.dom{i}.N);
                Z=matrix(p.nodes.Z(domain),p.dom{i}.M,p.dom{i}.N);
                F=matrix(domain,p.dom{i}.M,p.dom{i}.N);
                surf(xbdy,ybdy,zbdy,fbdy);
                surf(R*cos(th(1)),R*sin(th(1)),Z,F);
                surf(R*cos(th($)),R*sin(th($)),Z,F);
                h=[h gca().children(1:3)];
             end
             rect=p.dom{i}.rect;
             param3d(rect([1 2 2 1 1])*cos(th(1)),rect([1 2 2 1 1])*sin(th(1)),rect([3 3 4 4 3]))
             param3d(rect([1 2 2 1 1])*cos(th($)),rect([1 2 2 1 1])*sin(th($)),rect([3 3 4 4 3]))
             param3d(rect(1)*cos(th),rect(1)*sin(th),rect(3)*ones(th));
             param3d(rect(2)*cos(th),rect(2)*sin(th),rect(3)*ones(th));
             param3d(rect(1)*cos(th),rect(1)*sin(th),rect(4)*ones(th));
             param3d(rect(2)*cos(th),rect(2)*sin(th),rect(4)*ones(th));
             gca().children(1:6).foreground=gray;
        end
        data=h(1).data;
        for i=1:length(h)
            data.x=[data.x h(i).data.x]
            data.y=[data.y h(i).data.y]
            data.z=[data.z h(i).data.z]
            data.color=[data.color h(i).data.color]
        end
        delete(h(2:$))
        h=h(1);
        h.data=data;
        h.user_data=data.color;
        h.color_mode=-1;
        h.color_flag=3;
        h.cdata_mapping="direct";
        isoview on
        gca().rotation_angles = [60,-130]
        colorbar(fmin,fmax,[START START+NC-1]);
        
        drawnow
    end
    title(sprintf('%s, t=%.1f',fstring,t(ini)));
end

// common/loadModel.sci
// load parameters values and eventually simulation results
//
// Authors: 
// MOTTELET S. (UTC), stephane.mottelet@utc.fr
// LETURIA M. (UTC)
// MANAI M. S. (UTC)
//
// This file is hereby licensed under the terms of the GNU GPL v3.0,
// For more information, see the COPYING file which you should have received// 
// Last modified : 30-Aug-2023

function loadModel
    [filename, pathname]=uigetfile("*.sod","","Select a model file");
    if ~isempty(filename)
        f=gcf();
        bt=findobj(f,"tag","load");
        bt.Enable="off";
        load(fullfile(pathname,filename),"p")
        param=f.UserData.param;
        p.model=param.model;
        f.user_data.param=p;
        common.refresh
        model_2Dbd.initialize
        if isfield(p,"sol")
             model_2Dbd.plot_results;
        end
        bt.Enable="on";
    end
end

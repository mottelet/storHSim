// Authors: 
//
// MOTTELET S. (UTC), stephane.mottelet@utc.fr
//
// This file is hereby licensed under the terms of the GNU GPL v3.0,
// For more information, see the COPYING file which you should have received
//
// Last modified : Thu Jul 20 11:49:34 CEST 2023

function stat=odeStat(t,y,yp,flag,stats,bt,tmax)
  f=gcf();
  if flag == 'init'
    waitbar(0,sprintf('Solving with IDA on [0,%g], current solver time = %.3f',tmax,0),bt);
  elseif flag == 'done'
  else
    waitbar(t/tmax,sprintf('Solving with IDA on [0,%g], current solver time = %.3f',tmax,t),bt);
  end
  stat=f.UserData.param.stop;
end

// common/refresh.sci
// 2D axisymmetric simulation : refresh parameter values in the gui
//
// Authors: 
// MANAI M. S. (AAQIUS, UTC), s.manai@aaqius.com
// MOTTELET S. (UTC), stephane.mottelet@utc.fr
// LETURIA M. (UTC), mikel.leturia@utc.fr
// 
// Last modified : 30-Aug-2023

function refresh()
  f=gcf();
  param=f.UserData.param;
  fields=f.UserData.fields;
  for i=1:size(fields,1)
    h=findobj(f,'tag',fields{p.file i,1});
    try
      value=param(fields{i,1});
      if isnan(value)
        set(h,'enable','off');
      else
        set(h,'string',string(param(fields{i,1})));
      end
    catch
    end
  end
//  h=findobj(f,'tag','scenario');
//  h.Value=find(strcmp(h.String,param.scenario));
end

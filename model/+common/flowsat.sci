// flowsat.sci
// Compute equilibrium pressure
//
// Authors: 
//
// MOTTELET S. (UTC), stephane.mottelet@utc.fr
// LETURIA M. (UTC)
// MANAI M. S. (UTC)
//
// This file is hereby licensed under the terms of the GNU GPL v3.0,
// For more information, see the COPYING file which you should have received//
// This file is hereby licensed under the terms of the GNU GPL v3.0,
// For more information, see the COPYING file which you should have received
//
// Last modified : Thu Jul 20 11:49:34 CEST 2023

function Qm=flowsat(t,P_v,p)
  [P_in,flowmax]=p.P_in_flow_function(t,p);
  // the 0.001*(P_v-P_in) mimics a "flow as a function of pressure drop" relationship
  Qm=flowmax.*tanh(0.0001*(P_v-P_in)./flowmax);
end

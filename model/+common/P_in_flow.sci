// common/P_in_flow.sci
// 
// Default input pressure and maximum flow a fonction of time
//
// Authors: 
//
// MOTTELET S. (UTC), stephane.mottelet@utc.fr
// LETURIA M. (UTC)
// MANAI M. S. (UTC)
//
// This file is hereby licensed under the terms of the GNU GPL v3.0,
// For more information, see the COPYING file which you should have received//
// This file is hereby licensed under the terms of the GNU GPL v3.0,
// For more information, see the COPYING file which you should have received
//
// Last modified : Thu Jul 20 11:49:34 CEST 2023

function [P_in,maxflow]=P_in_flow(t,p)  
  P_in=p.P_in*common.smootheststep(t);
  maxflow=p.flow*common.smootheststep(t).*(1-common.smootheststep(t-p.t_closing));
end
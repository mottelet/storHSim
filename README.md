# StorHSim

This is the Scilab version of code used in this paper

```bibtex
@article{manai:hal-02459251,
  TITLE = {{Comparative study of different storage bed designs of a solid-state hydrogen tank}},
  AUTHOR = {Manai, Mohamed Sakreddine and Leturia, Mikel and Pohlmann, Carsten and Oubraham, Jorn and Mottelet, St{\'e}phane and Levy, Michael and Saleh, Khashayar},
  URL = {https://hal.science/hal-02459251},
  JOURNAL = {{Journal of Energy Storage}},
  PUBLISHER = {{Elsevier}},
  VOLUME = {26},
  PAGES = {101024},
  YEAR = {2019},
  MONTH = Dec,
  DOI = {10.1016/j.est.2019.101024},
  PDF = {https://hal.science/hal-02459251/file/S2352152X19306504.pdf},
  HAL_ID = {hal-02459251},
  HAL_VERSION = {v1},
}
```
The simulation runs in the latest version of Scilab (https://www.scilab.org/download/scilab-2024.0.0).

## How to run 
Download the latest [release](https://gitlab.com/mottelet/storHSim/-/releases), uncompress, then launch Scilab and run the main script:
```
--> cd model
--> exec("+model_2Dbd/main.sci")
```

## Screenshot

![Application screenshot](images/app.png)

## License

Files in this repository are hereby licensed under the terms of the GNU GPL v3.0.
